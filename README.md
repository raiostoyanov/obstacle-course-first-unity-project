Obstacle Course is the first thing I tried to do, as I installed Unity on my PC and it is a really, really simple game.
All you need to do is to move Dodgy(The Player Tile) around with the arrow keys or W, A, S, D and try to make it to the Finish Tile Without hitting anything. Everything you bump into, turns red, so it lets you know, you hit that object already.
Almost no functionality at all. With this experiment, my goal was to get to know my way around the Unity system.

P.S. The edges of the arena are a bit buggy, you can fall into the void. :)
