using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorer : MonoBehaviour
{
    int hitsCount = 0;

    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.tag != "Hit")
        {
        hitsCount++;
        Debug.Log("You've bumped into a thing this many times: " + hitsCount);
        }
    }
}
